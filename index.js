'use strict';

module.exports = {
  extends: [
    './rules/base.yml',
    './rules/styles.yml'
  ]
};
